# -*- coding: utf-8 -*-

from odoo import models, fields, api


class daily_windspeed(models.Model):

	_name = 'daily.windspeed'

	gsid = fields.Integer('GSID')
	formal_name = fields.Char('Formal name')
	district = fields.Char('District')
	station_id = fields.Integer('Station Id')
	year = fields.Integer('Year')
	month = fields.Integer('Month')
	days = fields.Integer('Days')
	wind_speed = fields.Float('Daily. windspeed')


class daily_rainfall(models.Model):

	_name = 'daily.rainfall'

	gsid = fields.Integer('GSID')
	formal_name = fields.Char('Formal name')
	district = fields.Char('District')
	station_id = fields.Integer('Station Id')
	year = fields.Integer('Year')
	month = fields.Integer('Month')
	days = fields.Integer('Days')
	rain_fall = fields.Float('Daily. rainfall')


class sunshine(models.Model):

	_name = 'sunshine.sunshine'

	gsid = fields.Integer('GSID')
	formal_name = fields.Char('Formal name')
	district = fields.Char('District')
	station_id = fields.Integer('Station Id')
	year = fields.Integer('Year')
	month = fields.Integer('Month')
	days = fields.Integer('Days')
	sum_sunshine = fields.Float('Daily. sunshine')


# class weather(models.Model):
#     _name = 'weather.weather'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100